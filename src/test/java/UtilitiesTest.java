import static org.junit.Assert.*;

public class UtilitiesTest
{

    @org.junit.Before
    public void setUp() throws Exception
    {
    }

    @org.junit.After
    public void tearDown() throws Exception
    {
    }

    @org.junit.Test
    public void everyNthCharNSmaller()
    {
        Utilities util = new Utilities();
        char[] output = util.everyNthChar(new char[]{'h', 'e', 'l', 'l', 'o'},2);
        assertArrayEquals(new char[]{'e', 'l'}, output);
    }

    @org.junit.Test
    public void everyNthCharNBigger()
    {
        Utilities util = new Utilities();
        char[] output = util.everyNthChar(new char[]{'h', 'e', 'l', 'l', 'o'},6);
        assertArrayEquals(new char[]{'h','e', 'l', 'l', 'o'}, output);
    }

    @org.junit.Test
    public void removePairsAABCDDEEFF()
    {
        Utilities util = new Utilities();
        assertEquals("ABCDEF", util.removePairs("AABCDDEEFF"));
    }

    @org.junit.Test
    public void removePairsABCCABDEEF()
    {
        Utilities util = new Utilities();
        assertEquals("ABCABDEF", util.removePairs("ABCCABDEEF"));
    }

    @org.junit.Test
    public void removePairsNull()
    {
        Utilities util = new Utilities();
        assertNull("Did not get null when I passed an argument of null", util.removePairs(null));
    }

    @org.junit.Test
    public void converter()
    {
        Utilities util = new Utilities();
        assertEquals(300, util.converter(10,5));
    }

    @org.junit.Test(expected=ArithmeticException.class)
    public void converterArithmeticException()
    {
        Utilities util = new Utilities();
        util.converter(10,0);
    }

    @org.junit.Test
    public void nullIfOddLengthOdd()
    {
        Utilities util = new Utilities();
        assertNull(util.nullIfOddLength("odd"));
    }

    @org.junit.Test
    public void nullIfOddLengthEven()
    {
        Utilities util = new Utilities();
        assertNotNull(util.nullIfOddLength("even"));
    }
}